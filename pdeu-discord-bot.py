#!/usr/bin/env python3

"""PDEU Discord Chat Bot

This is the main file for the chat bot developed for the PlayDateEU community.
"""

import discord
from discord.ext.commands import Bot


bot = Bot(
    command_prefix="!",
    activity=discord.Game(name="Commands: !help"),
    case_insensitive=True,
    max_messages=10_000
)


def retrieve_token():
    """Retrieves the token from a file."""
    with open("token", "r") as token_file:
        line = token_file.readline().strip()
    return line


if __name__ == "__main__":
    print("The bot is starting up.")
    bot.load_extension("cogs.nice")
    bot.load_extension("cogs.currency")
    bot.load_extension("cogs.ravensays")
    print("All Cogs have been loaded.")
    bot.run(retrieve_token())
