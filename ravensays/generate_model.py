#!/usr/bin/env python3

import markovify
import os

PATH_TO_JSON = "ravensays/ravenholdt.json"
PATH_TO_CORPUS = "ravensays/ravenholdt.txt"


def get_model():
    """Check the current directory for pre-generated model and load it if it exists
    otherwise generate the model and save it into a json for future use
    """
    if os.path.isfile(PATH_TO_JSON):
        with open(PATH_TO_JSON, 'r') as f:
            model_json = f.read()
    else:
        with open(PATH_TO_CORPUS, 'r') as f:
            text_model = markovify.Text(f.read())
            model_json = text_model.to_json()
        # save the model for future use
        with open(PATH_TO_JSON, 'w') as f:
            f.write(model_json)

    return model_json


def generate_sentence():
    return model.make_sentence()


model = markovify.Text.from_json(get_model())
