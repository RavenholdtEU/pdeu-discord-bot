#!/usr/bin/env python3

"""This is the listener used for automatically converting between currencies.
"""

import discord
from discord.ext import commands
from currency.validate_convert import convert_from_message


class Currency(commands.Cog):

    def __init__(self, bot):
        self.bot = bot

    @commands.Cog.listener()
    async def on_message(self, message: discord.Message):
        # Make sure the bot does not respond to itself
        if message.author == self.bot.user:
            return

        # call the conversion function, display results only if it actually returns something
        converted_message = convert_from_message(message.content)
        if converted_message:
            await message.channel.send(pretty_print_for_discord(converted_message))
        else:
            pass


def pretty_print_for_discord(converted_msg):
    """Takes the list with converted currencies and builds a nice message string for discord."""
    backticks = '```'
    output = []

    for item in converted_msg:  # for each dictionary in the list
        output.append(backticks)  # begin with backticts
        number, orig_curr = item['orig_currency']
        rounded_orig = round(float(number), 2)
        output.append(f'{rounded_orig} {orig_curr} is: ')  # and start string with the orig currency

        for currency in item.items():  # then for each converted currency
            if currency[0] != 'orig_currency':
                rounded = round(currency[1], 2)  # round the value of the converted currency
                output.append(f'{rounded} {currency[0]}  ')  # and append it after the original
        output.append(backticks)  # finally enclose the set for one converted pair in backtics again

    # transform the resulting list into one string
    result = "".join(output)

    return result


def setup(bot):
    bot.add_cog(Currency(bot))
    print("Cog loaded: Currency")
