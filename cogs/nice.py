#!/usr/bin/env python3

"""This is the listener that replies to Patropolis' nice with a random selection
from the answers list.
"""

import discord
from discord.ext import commands
from random import choice


class Nice(commands.Cog):

    def __init__(self, bot):
        self.bot = bot

    @commands.Cog.listener()
    async def on_message(self, message: discord.Message):
        # Make sure the bot does not respond to itself
        if message.author == self.bot.user:
            return

        if message.content == "I for one welcome our AI overlords.":
            await message.channel.send("Very well, you will be killed last, {0}!".format(message.author))

        nice_list = ["nice", "nice.", "nice!", "nice "]
        if message.content.lower() in nice_list and str(message.author) == "Patropolis55#7226":
            await message.channel.send(reply_to_patropolis_french())

"""
def reply_to_patropolis():
    replies = [
        "Good craic!",
        "That's grand.",
        "What a great bunch of lads!",
        "I'm pooping in the embassy.",
        "Tiocfaidh ár lá!",
        "Noice!",
        "This will help my lactic acid.",
        "Grand.",
        "That's pretty rock and roll.",
        "nice",
        "I am so warm right now.",
        "God I love girls.",
        "ugh",
        "I have an itchy bum.",
        "I just had an awesome poop, lads!",
        "Boli me kurac.",
        "[F1 trivia]",
        "...and don't call me paddy!",
        "That was a solid 6 on the Bristol scale.",
        "I too have a Japanese waifu",
        "SweepyB",
        "I love getting blasted.",
        "This is good vibes",
        "fuckin class",
        "Right on, brother.",
        "that was pretty mad, I enjoyed it",
        "you're a good lad",
        "I eat fish and chips on the bus.",
        "I just want to stick my 3.5mm somewhere",
        "I'd wank to Elusive",
        "who let this many weebs in here"
    ]
"""

def reply_to_patropolis_french():
    replies = [
        "Bon craic!",
        "C'est grandiose.",
        "Quelle belle bande de gars!",
        "Je fais caca à l'ambassade.",
        "Tiocfaidh ár lá!",
        "l'Noice!",
        "Cela aidera mon acide lactique.",
        "Grandiose.",
        "C'est plutôt rock and roll.",
        "joli",
        "J'ai tellement chaud en ce moment.",
        "Dieu j'aime les filles.",
        "pouah...",
        "J'ai des fesses qui piquent.",
        "Je viens d'avoir une merde géniale, gars!",
        "Boli me le kurac.",
        "[Trivia de F1]",
        "... et ne m'appelle pas « paddy! »",
        "C'était un solide 6 sur l'échelle de Bristol.",
        "Moi aussi j'ai un waifu japonais",
        "l'SviaupiB",
        "J'adore me faire exploser.",
        "Ce sont de bonnes vibrations",
        "putain de classe",
        "Tout de suite, frère.",
        "c'était assez fou, j'ai bien aimé",
        "tu es un bon garçon",
        "Je mange du poisson-frites dans le bus.",
        "Je veux juste coller mon 3,5 mm quelque part",
        "Je me branlerais à monsieur Elusíve",
        "qui a laissé autant de weebs ici"
    ]


    return choice(replies)


def setup(bot):
    bot.add_cog(Nice(bot))
    print("Cog loaded: Nice")
